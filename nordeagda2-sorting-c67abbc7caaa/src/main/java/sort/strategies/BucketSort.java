package sort.strategies;

import sort.AbstractSortStrategy;
import sort.PorownywarkaAlgorytmow;

public class BucketSort extends AbstractSortStrategy {
    public BucketSort() {
        super();
    }

    public void sort() {
        int counter = 0;
        // TODO: sortowanie bucket sort
        int[] kubelki = new int[PorownywarkaAlgorytmow.PROG_WARTOSCI];
        for (int i = 0; i < array.length; i++) {
            kubelki[array[i]]++;
            counter++;
        }

        int indeksLiczb = 0;
        for (int i = 0; i < PorownywarkaAlgorytmow.PROG_WARTOSCI; i++) {
            for (int j = 0; j < kubelki[i]; j++) {
                array[indeksLiczb++] = i;
                counter++;
            }
        }
        System.out.println(counter);
//        System.out.println("Posortowano liczb :" + indeksLiczb);
    }
}
